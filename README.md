# gitlab merge request script

This is a script that makes merge request using current branch as the source
branch, to other remote branch.

# getting started

for script to work you must create gitlab access token and save it to
environment variable `GITLAB_PRIVATE_TOKEN`. to create gitlab access token go to
https://gitlab.com/profile/personal_access_tokens if you need help setting up
environment variable https://askubuntu.com/a/849954
