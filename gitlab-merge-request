#!/bin/bash

source "$(dirname $0)/config"

# ./config variables:
# DEFAULT_TARGET_BRANCH = "master"
# ASK_FOR_TARGET_BRANCH = 0 or 1
# DEFAULT_ASSIGNEE = "gitlab_username"

current_project=$(git config --get remote.origin.url)
if [[ ! $current_project ]]; then
  echo Error: you are calling merge script outside of the gitlab repository, please make sure you are inside the Gitlab project before calling merge script
  exit 1
fi


if [[ ! $GITLAB_PRIVATE_TOKEN ]]; then
  echo for script to work please save gitlab access token to the "GITLAB_PRIVATE_TOKEN" enviroment variable
  echo to create gitlab access token go to https://gitlab.com/profile/personal_access_tokens
  echo if you need help setting up environment variable https://askubuntu.com/a/849954
  exit 1
fi

if [[ ! $GITLAB_PROJECT_ID ]]; then
    echo Environment variable "GITLAB_PROJECT_ID" is not set, fetching project id for "$current_project"
    GITLAB_PROJECT_ID=$(curl --silent --header "PRIVATE-TOKEN: $GITLAB_PRIVATE_TOKEN" "https://gitlab.com/api/v4/projects?membership=true&simple=true" | sed -e 's/[{}]/\n/g' | grep $current_project | grep -oP '(?<="id":)\w+')
    if [[ ! $GITLAB_PROJECT_ID ]]; then
      echo "Error: Could not find gitlab project($current_project), be sure that you are calling script from inside your gitlab project"
      exit 1
    fi
fi

# Get current branch from GIT
branch_name=$(git symbolic-ref -q HEAD)
branch_name=${branch_name##refs/heads/}
branch_name=${branch_name:-HEAD}

BRANCH=$branch_name

remote_branches=$(curl --silent --header "PRIVATE-TOKEN: $GITLAB_PRIVATE_TOKEN" "https://gitlab.com/api/v4/projects/$GITLAB_PROJECT_ID/repository/branches")


if [[ ! $DEFAULT_TARGET_BRANCH ]]; then
  DEFAULT_TARGET_BRANCH="master"
fi
TARGET_BRANCH=$DEFAULT_TARGET_BRANCH

if echo $remote_branches | grep -q "\"name\":\"$BRANCH\"[,}]"; then
  echo remote branch exists
else 
  echo "Error: "
  echo "There is no remote branch named \"$BRANCH\""
  echo "you either didn't push branch \"$BRANCH\" to the remote repository," 
  echo "or remote branch name does not match local branch name." 
  echo "For script to work remote and local names must match!"
  echo "create remote branch with:"
  echo "git push origin $BRANCH"
  exit 1 
fi

if [[ $ASK_FOR_TARGET_BRANCH = true ]]; then
  echo -n "Enter target branch (default: $DEFAULT_TARGET_BRANCH): "
  read TARGET_BRANCH
  if [[ ! $TARGET_BRANCH ]]; then
    TARGET_BRANCH=$DEFAULT_TARGET_BRANCH
  fi
fi

echo "Merge Request will go from branch \"$BRANCH\" to branch \"$TARGET_BRANCH\"!"

echo -n "Enter your title for Merge Request: "
read merge_request_title
echo


ASSIGNEE_ID=""
POST_MERGE_REQUEST () {
  curl --header "Content-Type: application/json" --header "PRIVATE-TOKEN: $GITLAB_PRIVATE_TOKEN"  --request POST --data "{\"source_branch\": \"$BRANCH\", \"target_branch\": \"$TARGET_BRANCH\", \"assignee_id\": $ASSIGNEE_ID, \"title\": \"$merge_request_title\" }" "https://gitlab.com/api/v4/projects/$GITLAB_PROJECT_ID/merge_requests"

}

echo -n "Enter assignee username for Merge Request (default '$DEFAULT_ASSIGNEE', enter '!' for no assignee): "
read assignee_username
echo $assignee_username


if [[ $assignee_username == "!" ]]; then
  ASSIGNEE_ID="\"\""
  POST_MERGE_REQUEST
  echo
  exit 0
fi

project_members=$(curl --silent --header "PRIVATE-TOKEN: $GITLAB_PRIVATE_TOKEN" "https://gitlab.com/api/v4/projects/$GITLAB_PROJECT_ID/members") 


if [[ ! $assignee_username ]]; then
  assignee_username=$DEFAULT_ASSIGNEE
fi
ASSIGNEE_ID=$(echo $project_members | sed -e 's/[{}]/\n/g' | grep "\"username\"\:\"$assignee_username\"" | grep -oP '(?<="id":)\w+')
if [[ ! $ASSIGNEE_ID ]]; then
  echo Error:
  echo Could not find user "$assignee_username!"
  echo Current project members:
  echo $project_members | sed -e 's/[,{}]/\n/g'| sed -e 's/\"//g' | grep "username" | awk -F':'  '{ print "\t"$2}'
  exit 1
fi


POST_MERGE_REQUEST
echo
exit 0
